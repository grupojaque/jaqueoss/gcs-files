module.exports = class GCSFileError extends Error {

  constructor(status, message, errorDetail) {
    super(message);
    this.name = this.constructor.name;
    this.status = status;
    this.errorDetail = errorDetail;
    Error.captureStackTrace(this, this.constructor);
  }
};
